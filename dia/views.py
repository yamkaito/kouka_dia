import logging
from django.views import generic
from .forms import InquiryForm
from django.urls import reverse_lazy
# Create your views here.

class IndexView(generic.TemplateView):
    template_name = "index.html"

class InquiryView(generic.FormView):
    template_name = "inquiry.html"
    form_class = InquiryForm
    success_url = reverse_lazy('dia:inquiry')

    def form_valid(self, form):
        form.send_email()
        return super().form_valid(form)